﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3.CollisionLibrary
{
    public static class Collision
    {
        private static Random random = new Random();
        public static string GenerateRandomString(int length = 6)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(p => p[random.Next(p.Length)]).ToArray());
        }
        public static void FindCollision(IHashFunction hashFunction)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            List<string> previous_keys = new List<string>();
            int successful_hashes = 0;

            while(true)
            {
                string random_string = GenerateRandomString(3);
                //Console.WriteLine($"random_key = {random_string}");

                string hashed_key = hashFunction.GetHash(random_string);

                if (!previous_keys.Contains(hashed_key))
                {
                    previous_keys.Add(hashed_key);
                    successful_hashes++;
                }
                else
                    break;
            }
            Console.WriteLine($"tries: {successful_hashes} before collision");
            watch.Stop();
            Console.WriteLine($"Time: {watch.Elapsed}");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab3.SHA256Library
{
    public class SHA256_CTX
    {
        public byte[] data { get; set; }
        public uint datalen { get; set; }
        public uint[] bitlen { get; set; }
        public uint[] state { get; set; }
    }
}

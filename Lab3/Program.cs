﻿using System;
using Lab3.CollisionLibrary;
using Lab3.KupynaLibrary;
using Lab3.SHA256Library;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            string key = "This is my super key";
            IHashFunction sHA256 = new SHA256();
            var watch = System.Diagnostics.Stopwatch.StartNew();
            sHA256.GetHash(key);
            watch.Stop();
            Console.WriteLine($"---Sha256\n Time: {watch.Elapsed}");

            Collision.FindCollision(new SHA256());

            Kupyna kupyna = new Kupyna();
            var watch2 = System.Diagnostics.Stopwatch.StartNew();
            kupyna.GetHash(key);
            watch2.Stop();
            Console.WriteLine($"---Kupyna\n Time: {watch2.Elapsed}");

            Collision.FindCollision(new Kupyna());
        }
    }
}
